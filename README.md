# 鼠标贪吃蛇
[![中文编程](https://gitee.com/Program-in-Chinese/overview/raw/master/%E4%B8%AD%E6%96%87%E7%BC%96%E7%A8%8B.svg)](https://gitee.com/Program-in-Chinese/overview)
#### 介绍
可以用鼠标操作方向的贪吃蛇
使用鼠标点击屏幕 蛇会自动向目标点移动
随着等级的提高 蛇本身会慢慢变胖

![截图1](https://images.gitee.com/uploads/images/2020/0725/150834_997d26e8_1148238.png "截图1.png")

![截图2](https://images.gitee.com/uploads/images/2020/0725/150920_5aaf1daf_1148238.png "截图2.png")

![运行演示](https://images.gitee.com/uploads/images/2020/0725/150941_8587f6b2_1148238.gif "截图3.gif")